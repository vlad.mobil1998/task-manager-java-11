package ru.amster.tm.api.servise;

import ru.amster.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCOMMANDS();

    String[] getARGUMENTS();

}
