package ru.amster.tm.api.repository;

import ru.amster.tm.model.Command;


public interface ICommandRepository {

    String[] getCommands(Command... value);

    String[] getArguments(Command... value);

    Command[] getTerminalCommands();

    String[] getCOMMANDS();

    String[] getARGUMENTS();

}
