package ru.amster.tm.api.repository;

import ru.amster.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}
