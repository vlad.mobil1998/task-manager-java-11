package ru.amster.tm.bootstrap;

import ru.amster.tm.api.controller.ICommandController;
import ru.amster.tm.api.controller.IProjectController;
import ru.amster.tm.api.controller.ITaskController;
import ru.amster.tm.api.repository.ICommandRepository;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.servise.ICommandService;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.constant.ProgramArgConst;
import ru.amster.tm.constant.TerminalCmdConst;
import ru.amster.tm.controller.CommandController;
import ru.amster.tm.controller.ProjectController;
import ru.amster.tm.controller.TaskController;
import ru.amster.tm.repository.CommandRepository;
import ru.amster.tm.repository.ProjectRepository;
import ru.amster.tm.repository.TaskRepository;
import ru.amster.tm.service.CommandService;
import ru.amster.tm.service.ProjectService;
import ru.amster.tm.service.TaskService;
import ru.amster.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public final void run(final String[] args) {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        while (true) parseCmd(TerminalUtil.nextLine());
    }

    private void parseCmd(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalCmdConst.HELP:
                commandController.showHelpMsg();
                break;
            case TerminalCmdConst.ABOUT:
                commandController.showAboutMsg();
                break;
            case TerminalCmdConst.VERSION:
                commandController.showVersionMsg();
                break;
            case TerminalCmdConst.INFO:
                commandController.showInfoMsg();
                break;
            case TerminalCmdConst.ARGUMENTS:
                commandController.showArgMsg();
                break;
            case TerminalCmdConst.COMMANDS:
                commandController.showCmdMsg();
                break;
            case TerminalCmdConst.EXIT:
                commandController.exit();
                break;
            case TerminalCmdConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalCmdConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalCmdConst.TASK_CLEAR:
                taskController.clearTask();
                break;
            case TerminalCmdConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalCmdConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalCmdConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            default:
                System.out.println("ERROR: Command not late");
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        for (int i = 0; i < args.length; i += 1) {
            System.out.println(" ");
            switch (args[i]) {
                case ProgramArgConst.HELP:
                    commandController.showHelpMsg();
                    break;
                case ProgramArgConst.ABOUT:
                    commandController.showAboutMsg();
                    break;
                case ProgramArgConst.VERSION:
                    commandController.showVersionMsg();
                    break;
                case ProgramArgConst.INFO:
                    commandController.showInfoMsg();
                    break;
                default:
                    System.out.println("ERROR: Argument not late");
                    break;
            }
        }
        return true;
    }

}
