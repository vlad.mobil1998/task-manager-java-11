package ru.amster.tm.constant;

public class TerminalCmdConst {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

    public static final String ARGUMENTS = "arguments";

    public static final String COMMANDS = "commands";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_CLEAR = "task-clear";

    public static final String TASK_CREATE = "task-create";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_CREATE = "project-create";

}
