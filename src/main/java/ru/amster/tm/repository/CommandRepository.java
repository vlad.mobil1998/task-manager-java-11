package ru.amster.tm.repository;

import ru.amster.tm.api.repository.ICommandRepository;
import ru.amster.tm.constant.ProgramArgConst;
import ru.amster.tm.constant.TerminalCmdConst;
import ru.amster.tm.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            TerminalCmdConst.HELP, ProgramArgConst.HELP, " - Display terminal commands."
    );

    private static final Command ABOUT = new Command(
            TerminalCmdConst.ABOUT, ProgramArgConst.ABOUT, " - Show developer info."
    );
    private static final Command INFO = new Command(
            TerminalCmdConst.INFO, ProgramArgConst.INFO, " - Display information about system."
    );
    private static final Command VERSION = new Command(
            TerminalCmdConst.VERSION, ProgramArgConst.VERSION, " - Show version info."
    );

    private static final Command EXIT = new Command(
            TerminalCmdConst.EXIT, null, " - Close application."
    );

    private static final Command COMMAND = new Command(
            TerminalCmdConst.COMMANDS, null, " - Display terminal command"
    );

    private static final Command ARGUMENT = new Command(
            TerminalCmdConst.ARGUMENTS, null, " - Display arguments program"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalCmdConst.TASK_CREATE, null, "Create new task"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalCmdConst.TASK_CLEAR, null, "Remove all task"
    );

    private static final Command TASK_LIST = new Command(
            TerminalCmdConst.TASK_LIST, null, "Show task list"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalCmdConst.PROJECT_CREATE, null, "Create new project"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalCmdConst.PROJECT_CLEAR, null, "Remove all project"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalCmdConst.PROJECT_LIST, null, "Show project list"
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, COMMAND, ARGUMENT,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGUMENTS = getArguments(TERMINAL_COMMANDS);

    public String[] getCommands(Command... value) {
        if (value == null || value.length == 0) return new String[]{};
        final String[] result = new String[value.length];
        int index = 0;
        for (int i = 0; i < value.length; i++) {
            final String name = value[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }

        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArguments(Command... value) {
        if (value == null || value.length == 0) return new String[]{};
        final String[] result = new String[value.length];
        int index = 0;
        for (int i = 0; i < value.length; i++) {
            final String arg = value[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }

        return Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCOMMANDS() {
        return COMMANDS;
    }

    public String[] getARGUMENTS() {
        return ARGUMENTS;
    }

}
