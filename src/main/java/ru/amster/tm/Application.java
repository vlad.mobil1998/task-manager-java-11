package ru.amster.tm;

import ru.amster.tm.bootstrap.Bootstrap;


public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
